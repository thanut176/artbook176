@extends('layouts.app')
@section('content')
    <br>
    <div style="text-align: center">
        <img src="{{ url('uploads/'.$todo->file_name) }}" width="80%" height="80%">
    </div>
    <br>

    <h1>ชื่อภาพ : {{$todo->title}}</h1>
    <p>รายละเอียด : {{$todo->content}}</p>
    <p>เทคนิค : {{$todo->technique}}</p>
    <p>ราคาซื้อ : {{$todo->sell}}</p>
    <p>ราคาเช่า : {{$todo->hire}}</p>
    <p>ข้อมูลติดต่อ : {{$todo->information}}</p>
    <form action=" {{ url('/todo/'.$todo->id) }}" method="post" id="form-delete">
        @method('DELETE')
        @csrf

    </form>
    <button class="btn btn-danger" onclick="confirm_delete()" type="button">Delete</button>
    <a href="{{ url('/todo/'.$todo->id.'/edit') }}" class="btn btn-primary">Edit</a>
    <a href="{{ url('/') }}" class="btn btn-secondary">Go Back</a>
    <br>
    <br>





    <script>
        function confirm_delete() {
            var text = '{!! $todo->title !!}';
        var confirm = window.confirm('นั่งยันการลบ' + text);
        if (confirm){
            document.getElementById('form-delete').submit();
        }

        }
    </script>
@endsection
