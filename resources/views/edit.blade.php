@extends('layouts.app')
@section('content')
    <h1>แก้ไขข้อมูล</h1>
    <form method="post" action="{{ url('/todo/'.$todo->id) }}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>ชื่อภาพ</label>
            <input type="text" name="title" class="form-control" value="{{ $todo->title }}">
        </div>
        <div class="form-group">
            <label>รายละเอียยด</label>
            <input type="text" name="content" class="form-control" value="{{ $todo->content }}">
        </div>

        <div class="form-group">
            <label>เทคนิค</label>
            <input type="text" name="technique" class="form-control" value="{{ $todo->technique }}" >
        </div>

        <div class="form-group">
            <label>ราคาซื้อ</label>
            <input type="text" name="sell" class="form-control" value="{{ $todo->sell }}" >
        </div>

        <div class="form-group">
            <label>ราคาขาย</label>
            <input type="text" name="hire" class="form-control" value="{{ $todo->hire }}">
        </div>

        <div class="form-group">
            <label>ข้อมูลการติดต่อ</label>
            <input type="text" name="information" class="form-control" value="{{ $todo->information }}">
        </div>



        <button  class="btn btn-success" type="submit">submit</button>

        <a href="{{ url('/') }}" class="btn btn-secondary">Go Back</a>
    </form>
@endsection
