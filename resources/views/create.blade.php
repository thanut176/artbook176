@extends('layouts.app')
@section('content')
    <div class="container">
    <h1>ลงขาย-เช่ารูปภาพ</h1>
    <form method="post" action="{{ url('/todo') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>ชื่อภาพ</label>
            <input type="text" name="title" class="form-control" >
        </div>
        <div class="form-group">
            <label>รายละเอียยด</label>
            <input type="text" name="content" class="form-control" >
        </div>

        <div class="form-group">
            <label>เทคนิค</label>
            <input type="text" name="technique" class="form-control" >
        </div>

        <div class="form-group">
            <label>ราคาขาย</label>
            <input type="text" name="sell" class="form-control" >
        </div>

        <div class="form-group">
            <label>ราคาเข่า</label>
            <input type="text" name="hire" class="form-control" >

        </div>

        <div class="form-group">
            <label>ข้อมูลการติดต่อ</label>
            <input type="text" name="information" class="form-control" >
        </div>

        <div class="form-group">
            <label>รูปภาพ</label>
            <input type="file" name="file" class="form-control" >
        </div>



        <button type="submit" class="btn btn-primary">submit</button>
    </form>
    </div>
@endsection


