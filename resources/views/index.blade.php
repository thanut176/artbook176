@extends('layouts.app')
@section('content')
    <br>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Tangerine">
    @if(count($todos)>0)
        @foreach($todos as $todo)



            <div class="card" style="width: 18rem; float: left; margin-left: 6 0px" >
                <img src="{{ url('uploads/'.$todo->file_name) }}" class="card-img-top" src="..." alt="Card image cap">
                <div class="card-body">

                        <h3>ชื่อภาพ : {{$todo->title}}</h3>

                    <p>รายละเอียด : {{$todo->content}}</p>
                    <p>เทคนิค : {{$todo->technique}}</p>
                    <p>ราคาซื้อ : {{$todo->sell}} บาท</p>
                    <p>ราคาขาย : {{$todo->hire}} บาท</p>

                    <a href="{{ url('/todo/'.$todo->id) }}" class="btn btn-primary" >ดูรายอะเอียด</a>
                </div>
            </div>



            @endforeach
    @endif
@endsection
