@extends('layouts.app')
@section('content')
    <div class="container">

            @if(count($posts) > 0)
                @foreach($posts as $post)
                <div class="card" style="width: 18rem; float: left " >
                    <img src="{{ url('uploads/'.$post->file_name) }}" class="card-img-top" src="..." alt="Card image cap">
                    <div class="card-body">

                        <h3>ชื่อภาพ : {{$post->title}}</h3>

                        <p>รายละเอียด : {{$post->content}}</p>
                        <p>เทคนิค : {{$post->technique}}</p>
                        <p>ราคาซื้อ : {{$post->sell}} บาท</p>
                        <p>ราคาขาย : {{$post->hire}} บาท</p>

                        <a href="{{ url('/todo/'.$post->id) }}" class="btn btn-primary" >ดูรายอะเอียด</a>
                    </div>
                </div>

                @endforeach
            @else
                <div class="text-center">
                    <p>ไม่พบข้อมูล</p>
                </div>
            @endif


@endsection
